// console.log("Hello world")

/*[ houseNumber, street, barangay, municipality, Province, zipCode ]
{ name, species, weight, measurement }*/

let num = 10;
const getCube = Math.pow(num,3);
console.log(`The cube of ${num} is ${getCube}.`);




let myAddress = [ 216, 'Zamora St.', 'Poblacion', 'Pilar', 'Bataan', 2101];

let [houseNumber, street, barangay, municipality, province, zipCode] = myAddress

console.log(`I live at ${houseNumber} ${street} ${barangay} ${municipality} ${province} ${zipCode}.`);




let animal = {
	name: "Lolong",
	kind: "Saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {name, kind, weight, measurement} = animal

console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)





let array = [25, 50, 75, 100];
array.forEach((arrayNum) => console.log(arrayNum));
	




let reduceNumber = array.reduce((x, y) => x+y);
console.log(reduceNumber);





class Dog{
	constructor(dogName, dogAge, dogBreed){
		this.name = dogName;
		this.age = dogAge;
		this.breed = dogBreed;
	}
}
let pet = new Dog("Chiquita", 7, "Chihuahua");
console.log(pet);